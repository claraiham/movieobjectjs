const movie = { // j'instancie un objet de type movie
    title: "Lord of the rings: The fellowship of the ring",// titre du film
    yearProduction: 2001, // année de sortie du film
    director: "Peter Jackson", // réalisateur
    actors : ["Viggo Mortensen", "Orlando Bloom", "Elijah Wood"], // acteurs principaux
    favoris: true, // favori mis à "vrai"
    note: "8,8/10", // note du film 
    duration: 0, // durée en minutes initialisée à 0
    changeDuration: function(hours, minutes){ // ma méthode prend en paramètres les heures et minutes
        this.duration = (hours*60)+minutes; // on utilise le mot clé this pour dire qu enous allons changer la valeur de la propriété qui suit ce mot clé  
    }    
}
movie.changeDuration(2, 58);// je passe les valeurs aux paramètres de la fonction ( ou méthode )
console.log(movie.duration);// j'appelle la propriété duration de mon objet, qui a été modifiée grâce à la fonction changeDuration